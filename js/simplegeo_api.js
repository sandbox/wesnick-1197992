(function ($) {

 
  Drupal.simplegeo = Drupal.simplegeo || {};
  
  Drupal.behaviors.simplegeo = {
    attach: function (context, settings) {
      
    
    
  
    }
  };
  
  Drupal.simplegeo.prototype = {};

  Drupal.simplegeo.getFeatureFromHandle = function(handle, callback) {
    
    var settings = response.settings || ajax.settings || Drupal.settings;
    
    var client = new simplegeo.Client(settings.simplegeo.api_key);
    
    client.getFeature(data.handle, function(err, feature) {
      if (err) {
        //console.log(err);
        return false;
      } else {
        var latlongs = [];
        for (i = 0; i < feature.geometry.coordinates[0].length; i++) { 
          latlongs[i] = [feature.geometry.coordinates[0][i][1],feature.geometry.coordinates[0][i][0] ];        
        }      
        callback(latlongs);     
      }    
    });  
    
  };
  /**
   * Command to close the simplegeo.
   */
  Drupal.simplegeo.getContextFromIP = function(callback) {
    // else, simplegeo context based on ip
    var client = new simplegeo.ContextClient(Drupal.settings.simplegeo.api_key);
    
    client.getLocationFromIP(function(err, position) {          
      if (err) {
        return false;
      } else {
        var params = {};
        params.lat = position.coords.latitude;
        params.lng = position.coords.longitude;
        callback(params);
      }
      
    });
  };
  
  /**
   * Command to display the simplegeo.
   */
  Drupal.simplegeo.featureFromHandle = function(ajax, response, status) {
  
    var settings = response.settings || ajax.settings || Drupal.settings;
  
    var client = new simplegeo.Client(settings.simplegeo.api_key);
    
    client.getFeature(data.handle, function(err, feature) {
      if (err) {
        //console.log(err);
        return false;
      } else {
        var latlongs = [];
        for (i = 0; i < feature.geometry.coordinates[0].length; i++) { 
          latlongs[i] = [feature.geometry.coordinates[0][i][1],feature.geometry.coordinates[0][i][0] ];        
        }      
        return latlongs;     
      }    
    });  
  
  };
  
  /**
   * Command to close the simplegeo.
   */
  Drupal.simplegeo.contextFromIP = function(ajax, response, status) {
    // else, simplegeo context based on ip
    var client = new simplegeo.ContextClient(Drupal.settings.simplegeo.api_key);
    
    client.getLocationFromIP(function(err, position) {          
      if (err) {
        return false;
      } else {
        var params = {};
        params.lat = position.coords.latitude;
        params.lng = position.coords.longitude;
        return params;
      }
      
    });
  };

})(jQuery);
