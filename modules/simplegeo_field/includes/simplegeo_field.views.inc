<?php

/**
 * @file
 *   Default views hooks.
 */

/**
 * Implements hook_views_plugins().
 */

function simplegeo_field_views_plugins() {
  $plugins = array(
    'module' => 'simplegeo_field',
    'style' => array(
      'simplegeo_field_map' => array(
        'title' => t('GMap with SimpleGeo Handles'),
        'help' => t('Displays a View as an GMap with SimpleGeo Handles.'),
        'handler' => 'simplegeo_field_plugin_style_map',
        'theme' => 'simplegeo_field_map',
        'theme path' => drupal_get_path('module', 'simplegeo_field') .'/includes',
        'path' => drupal_get_path('module', 'simplegeo_field') .'/includes',
        'uses fields' => TRUE,
        'uses row plugin' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );

  return $plugins;
}
