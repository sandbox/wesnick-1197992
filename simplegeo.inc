<?php

/*
 * @file SimpleGeo Classes
 *
 *
 */


/**
 * A remote endpoint type for invoking REST services.
 */
class SimpleGeoRESTEndpoint extends WSClientEndpoint {


  /**
   * @var HttpClient
   */
  protected $client;
  protected $key;
  protected $secret;

  public function __construct(WSClientServiceDescription $service) {
    $this->service = $service;
    $this->url = $service->url;
    $this->key  = variable_get('simplegeo_oauth', '');
    $this->secret = variable_get('simplegeo_secret', '');

  }

  /**
   * @return HttpClient
   */
  public function client() {
    if (!isset($this->client)) {
      // Create our consumer
      $consumer = new OAuthConsumer($this->key, $this->secret);

      // Blank token
      // @link http://developers.simplegeo.com/blog/2011/01/07/two-legged-oauth/
      $token = new OAuthToken("", "");

      // Create our HttpAuthentication object
      $authentication = new HttpClientOAuth($consumer, $token, new OAuthSignatureMethod_HMAC_SHA1(), TRUE, TRUE, $this->url);

      // Create our HttpClient object
      $this->client =  new HttpClient($authentication, new HttpClientBaseFormatter(HttpClientBaseFormatter::FORMAT_JSON));

      // Pass through additional curl options.
      if (!empty($this->service->settings['curl options'])) {
        $this->client->options['curlopts'] = $this->service->settings['curl options'];
      }
    }
    return $this->client;
  }

  /**
   * Calls the REST service.
   *
   * @param string $operation_name
   *   The name of the operation to execute.
   * @param array $arguments
   *   Arguments to pass to the service with this operation.
   */
  public function call($operation_name, $arguments) {

    $operation = $this->service->operations[$operation_name];
    $operation_url = isset($operation['url']) ? $operation['url'] : '';
    // Replace argument patterns in the operation URL.
    foreach ($arguments as $key => $value) {
      if (strpos($operation_url, '@' . $key) !== FALSE) {
        $operation_url = str_replace('@' . $key, $value, $operation_url);
        unset($arguments[$key]);
      }
      if (!isset($value)) {
        unset($arguments[$key]);
      }
    }
    $client = $this->client();
    $type = isset($operation['type']) ? $operation['type'] : 'GET';
    $data = NULL;
    if (isset($operation['data'])) {
      $data = $arguments[$operation['data']];
      unset($arguments[$operation['data']]);
    }
    try {
      if ($type == 'GET') {
        $response = $client->execute(new SimpleGeoClientRequest($this->service->url . '/' . $operation_url, array(
          'method' => $type,
          'parameters' => $arguments,
        )));
      }
      else {
        $response = $client->execute(new SimpleGeoClientRequest($this->service->url . '/' . $operation_url, array(
          'method' => $type,
          'parameters' => $arguments,
          'data' => $data,
        )));
      }

      return $response;
    }
    catch (HttpClientException $e) {
      //watchdog('simplegeo', 'Error invoking service', $e);
      //throw new WSClientException('Error invoking the REST service %name, operation %operation: %error', array('%name' => $this->service->label, '%operation' => $operation_name, '%error' => $e->getMessage()));
    }
  }



  public function entities() {
    if (!empty($this->service->settings['resources'])) {
      return $this->service->settings['resources'];
    }
  }

}

/**
 *  Overriding the HttpClientRequest Class to customize our url encoding,
 *  which doesn't use array brackets [], and to filter out our NULL values
 */
class SimpleGeoClientRequest extends HttpClientRequest {

  /**
   * Returns the url taken the parameters into account.
   */
  public function url() {
    if (empty($this->parameters)) {
      return $this->url;
    }
    $total = array();
    foreach ($this->parameters as $k => $v) {
      if (is_array($v)) {
        foreach ($v as $va) {
          if($va) {
            $total[] = HttpClient::urlencode_rfc3986($k) . "=" . HttpClient::urlencode_rfc3986($va);
          }
        }
      }
      else {
        if ($v) {
          $total[] = HttpClient::urlencode_rfc3986($k) . "=" . HttpClient::urlencode_rfc3986($v);
        }
      }
    }
    $out = implode("&", $total);
    return $this->url . '?' . $out;
  }
}



class SimplegeoRecord {

  private $properties;
  public $layer;
  public $id;
  public $created;
  public $lat;
  public $lng;

  public function __construct($layer, $id, $lat = NULL, $lng = NULL) {
    $this->layer = $layer;
    $this->id = $id;
    $this->lat = $lat;
    $this->lng = $lng;
    $this->properties = array();
    $this->created = REQUEST_TIME;
  }

  public function toArray() {
    return array(
      'type' => 'Feature',
      'id' => $this->id,
      'created' => $this->created,
      'geometry' => array(
        'type' => 'Point',
        'coordinates' => array($this->lng, $this->lat),
      ),
      'properties' => (object) $this->properties
    );
  }


  public function __set($key, $value) {
    $this->properties[$key] = $value;
  }

  public function __get($key) {
    return isset($this->properties[$key]) ? $this->properties[$key] : NULL;
  }
}