<?php
// $Id$

/**
 * @file
 * Plugin to provide a simplegeo geocoder.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("SimpleGeo Geocoder"),
  'description' => t('Geocodes via SimpleGeo API'),
  'callback' => 'simplegeo_geocoder',
  'field_types' => array('text','text_long','addressfield','text_with_summary'),
  'field_callback' => 'simplegeo_geocode_field',
);

/**
 * Geocode Address for geofield
 *
 */
function simplegeo_geocoder($address, $return = 'point') {

  $geometry = array();

  // Load up geoPHP to do the conversions
  $geophp = geofield_load_geophp();
  if (!$geophp) {
    drupal_set_message(t("Unable to load geoPHP library. Not all values will be computed correctly"), 'error');
    return;
  }

  if ($return == 'point') {

    $result = simplegeo_context_get_address(array('address' => $address));

    $lat = $result['query']['latitude'];
    $lon = $result['query']['longitude'];
    if (is_numeric($lat) && is_numeric($lon)) {
      $geometry = new Point(floatval($lon), floatval($lat));
    }

  }
  //@TODO: will have to implement features categories choice to select the item to be geocoded

//  elseif ($return == 'bounds') {
//    $values['top'];
//    $values['bottom'];
//    $values['right'];
//    $values['left'];
//
//
//    $points = array (
//      $this->getTopLeft($delta),
//      $this->getTopRight($delta),
//      $this->getBottomRight($delta),
//      $this->getBottomLeft($delta),
//    );
//    $outer_ring = new LineString($points);
//    return new Polygon(array($outer_ring));
//
//  }
//  elseif ($resurn == 'polygon') {
//
//
//  }

  return $geometry;

}

function simplegeo_geocode_field($field, $field_item) {

  if ($field['type'] == 'text' || $field['type'] == 'text_long' || $field['type'] == 'text_with_summary') {
  	return simplegeo_geocode($field_item['value']);
  }
  if ($field['type'] == 'addressfield') {
    // For simplegeo we MUST have the postal code, or at least more than the country

    if (empty($field_item['postal_code'])) {
      return;
    }

  	$address = '';
  	// Premise does not make sense, corresponds to Address2
  	//if (!empty($field_item['premise']))                 $address .= $field_item['premise'].',';
  	if (!empty($field_item['thoroughfare']))            $address .= $field_item['thoroughfare'].',';
  	if (!empty($field_item['locality']))                $address .= $field_item['locality'].',';
  	if (!empty($field_item['administrative_area']))     $address .= $field_item['administrative_area'].',';
    if (!empty($field_item['sub_administrative_area'])) $address .= $field_item['sub_administrative_area'].',';
    if (!empty($field_item['postal_code']))             $address .= $field_item['postal_code'].',';
    //if (!empty($field_item['country']))                 $address .= $field_item['country'].',';

    $address = rtrim($address,', ');
    return simplegeo_geocoder($address);
  }
}
