<?php



/**
 * Implements hook_wsclient_endpoint_types().
 */
function simplegeo_wsclient_endpoint_types() {
  return array(
    'simplegeo' => array(
      'label' => t('SimpleGeo REST'),
      'class' => 'SimpleGeoRESTEndpoint',
    ),
  );
}

/**
 * Implements hook_default_wsclient_service().
 */
function simplegeo_default_wsclient_service() {


  $service = new WSClientServiceDescription();
  $service->name = 'simplegeo';
  $service->label = 'Simple Geo';
  $service->url = 'http://api.simplegeo.com';

  $service->type = 'simplegeo';

  $service->datatypes = simplegeo_datatypes();

  /*
   * PLACES
   */

  // Standard parameter array for places endpoints
  //   q - A search term. For example, q=Starbucks would return all places matching the name Starbucks.
  //   category - Filter by an exact classifier (types, categories, subcategories, tags)
  //   radius - Search by radius in kilometers. Default and maximum value for radius is 25km.
  //   limit - The amount of features to return. Default is 25.

  $place_params = array(
    'q' => array('type' => 'text', 'label' => 'Query Term(s)'),
    'category' => array('type' => 'text', 'label' => 'Category'),
    'radius' => array('type' => 'integer', 'label' => 'Radius'),
    'limit' => array('type' => 'integer', 'label' => 'Number of Results'),
    'start' => array('type' => 'integer', 'label' => 'Result Number to Start From (pagination)'),
  );

  // OPS
  //    PLACES ENDPOINTS
  //    http://api.simplegeo.com/1.0/places/{lat},{lon}.json
  //    http://api.simplegeo.com/1.0/places/address.json?address={address}
  //    http://api.simplegeo.com/1.0/places/ip.json (no parameters, returns location from user's IP address)
  //    http://api.simplegeo.com/1.0/places/{ip}.json

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Places IP Search';
  $operation['url'] = '1.2/places/@ip_address.json';
  $operation['parameter']['ip_address'] = array('type' => 'text', 'label' => 'IP Address');
  $operation['parameter'] += $place_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_places', 'label' => 'Places Result');
  $service->operations['places_get_ip'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Places Lat/Lon Search';
  $operation['url'] = '1.2/places/@latitude,@longitude.json';
  $operation['parameter']['latitude'] = array('type' => 'text', 'label' => 'Latitude');
  $operation['parameter']['longitude'] = array('type' => 'text', 'label' => 'Longitude');
  $operation['parameter'] += $place_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_places', 'label' => 'Places Result');
  $service->operations['places_get_latlon'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Places Addy Search';
  $operation['url'] = '1.2/places/address.json';
  $operation['parameter']['address'] = array('type' => 'text', 'label' => 'Address');
  $operation['parameter'] += $place_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_places', 'label' => 'Places Result');
  $service->operations['places_get_address'] = $operation;
  $services[$service->name] = $service;


  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Places by Bounding Box';
  $operation['url'] = '1.2/places/@sw_lat,@sw_lon,@ne_lat,@ne_lon.json';
  $operation['parameter']['sw_lat'] = array('type' => 'decimal', 'label' => 'SW Latitude');
  $operation['parameter']['sw_lon'] = array('type' => 'decimal', 'label' => 'SW Longitude');
  $operation['parameter']['ne_lat'] = array('type' => 'decimal', 'label' => 'NE Latitude');
  $operation['parameter']['ne_lon'] = array('type' => 'decimal', 'label' => 'NE Longitude');
  $operation['parameter'] += $place_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_places', 'label' => 'Places Result');
  $service->operations['places_get_bounding'] = $operation;
  $services[$service->name] = $service;


  //    CONTEXT ENDPOINTS
  //    http://api.simplegeo.com/1.0/context/{lat},{lon}.json
  //    http://api.simplegeo.com/1.0/context/address.json?address={address}
  //    http://api.simplegeo.com/1.0/context/ip.json (no parameters, returns location from user's IP address)
  //    http://api.simplegeo.com/1.0/context/{ip}.json

  // Standard parameter array for context endpoints
  //  filter=demographics.acs.B08012
  $context_params = array(
    'filter' => array('type' => 'text', 'label' => 'Category'),
    'features__category' => array('type' => 'text', 'label' => 'Category Filter'),
    'features__subcategory' => array('type' => 'text', 'label' => 'Subcategory Filter'),
    'features__type' => array('type' => 'text', 'label' => 'Type Filter'),
  );


  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Context- IP Search';
  $operation['url'] = '1.0/context/@ip_address.json';
  $operation['parameter']['ip_address'] = array('type' => 'text', 'label' => 'IP Address');
  $operation['parameter'] += $context_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_context', 'label' => 'Context Result');
  $service->operations['context_get_ip'] = $operation;
  $services[$service->name] = $service;


  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Context- Lat/Lon Search';
  $operation['url'] = '1.0/context/@latitude,@longitude.json';
  $operation['parameter']['latitude'] = array('type' => 'decimal', 'label' => 'Latitude');
  $operation['parameter']['longitude'] = array('type' => 'decimal', 'label' => 'Longitude');
  $operation['parameter'] += $context_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_context', 'label' => 'Context Result');
  $service->operations['context_get_latlon'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Context- Addy Search';
  $operation['url'] = '1.0/context/address.json';
  $operation['parameter']['address'] = array('type' => 'text', 'label' => 'Address');
  $operation['parameter'] += $context_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_context', 'label' => 'Context Result');
  $service->operations['context_get_address'] = $operation;
  $services[$service->name] = $service;


  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Context - Features by Bounding Box';
  $operation['url'] = '1.0/context/@nw_lat,@nw_lon,@se_lat,@sw_lon.json';
  $operation['parameter']['sw_lat'] = array('type' => 'decimal', 'label' => 'SW Latitude');
  $operation['parameter']['sw_lon'] = array('type' => 'decimal', 'label' => 'SW Longitude');
  $operation['parameter']['ne_lat'] = array('type' => 'decimal', 'label' => 'NE Latitude');
  $operation['parameter']['ne_lon'] = array('type' => 'decimal', 'label' => 'NE Longitude');
  $operation['parameter'] += $context_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_context', 'label' => 'Context Result');
  $service->operations['context_get_bounding'] = $operation;
  $services[$service->name] = $service;


  //    FEATURES ENDPOINTS
  //    http://api.simplegeo.com/1.0/features/{handle}.json
  //    http://api.simplegeo.com/1.0/features/{handle}.png (displays a QR code that links to the JSON version of the endpoint)
  //    http://api.simplegeo.com/1.0/features/{handle}/annotations.json
  //    http://api.simplegeo.com/1.0/features/categories.json

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Feature Categories';
  $operation['url'] = '1.0/features/categories.json';
  $operation['result'] = array('type' => 'wsclient_simplegeo_categories', 'label' => 'Categories Result');
  $service->operations['categories'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Feature';
  $operation['url'] = '1.0/features/@handle.json';
  $operation['parameter']['handle'] = array('type' => 'text', 'label' => 'SG Handle');
  $operation['result'] = array('type' => 'wsclient_simplegeo_features', 'label' => 'Features Result');
  $service->operations['features_get_handle'] = $operation;
  $services[$service->name] = $service;


  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Feature QR';
  $operation['url'] = '1.0/features/@handle.png';
  $operation['parameter']['handle'] = array('type' => 'text', 'label' => 'SG Handle');
  $operation['result'] = array('type' => 'wsclient_simplegeo_features', 'label' => 'Features Result');
  $service->operations['features_get_handleQR'] = $operation;
  $services[$service->name] = $service;


  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Feature Annotations';
  $operation['url'] = '1.0/features/@handle/annotations.json';
  $operation['parameter']['handle'] = array('type' => 'text', 'label' => 'SG Handle');
  $operation['result'] = array('type' => 'wsclient_simplegeo_features', 'label' => 'Features Result');
  $service->operations['features_get_annotations'] = $operation;
  $services[$service->name] = $service;



  /*
   * LAYERS
   */
  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Layers - List all Layers';
  $operation['url'] = '0.1/layers.json';
  $operation['parameter']['limit'] = array('type' => 'integer', 'label' => 'Number of Layers to retrieve');
  $operation['parameter']['cursor'] = array('type' => 'text', 'label' => 'Pagination Cursor Parameter');
  $operation['result'] = array('type' => 'wsclient_simplegeo_layer', 'label' => 'Layer Result');
  $service->operations['layer_get_list'] = $operation;
  $services[$service->name] = $service;


  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Layer - Get Layer Detail';
  $operation['url'] = '0.1/layers/@layer.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['result'] = array('type' => 'wsclient_simplegeo_layer', 'label' => 'Layer Result');
  $service->operations['layer_get_single'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'PUT';
  $operation['label'] = 'Layer - Put Layer Detail';
  $operation['url'] = '0.1/layers/@layer.json';
  $operation['data'] = 'layerdata';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter']['layerdata'] = array('type' => 'layer', 'label' => 'Layer Data');
  $operation['result'] = array('type' => 'wsclient_simplegeo_layer', 'label' => 'Layer Result');
  $service->operations['layer_put_single'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'DELETE';
  $operation['label'] = 'Layer - Layer Detail';
  $operation['url'] = '0.1/layers/@layer.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['result'] = array('type' => 'wsclient_simplegeo_layer', 'label' => 'Layer Result');
  $service->operations['layer_delete_single'] = $operation;
  $services[$service->name] = $service;

  /*
   * RECORDS
   */

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Record - Get Record Detail';
  $operation['url'] = '0.1/records/@layer/@id.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter']['id'] = array('type' => 'text', 'label' => 'Record ID');
  $operation['result'] = array('type' => 'wsclient_simplegeo_record', 'label' => 'Record Result');
  $service->operations['record_get_single'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'PUT';
  $operation['label'] = 'Record - Put Single Record';
  $operation['url'] = '0.1/records/@layer/@id.json';
  $operation['data'] = 'record';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter']['id'] = array('type' => 'text', 'label' => 'Record ID');
  $operation['parameter']['record'] = array('type' => 'record', 'label' => 'Record');
  $operation['result'] = array('type' => 'wsclient_simplegeo_accepted', 'label' => '202');
  $service->operations['record_put_single'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'DELETE';
  $operation['label'] = 'Record - Delete Single Record';
  $operation['url'] = '0.1/records/@layer/@id.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter']['id'] = array('type' => 'text', 'label' => 'Record ID');
  $operation['result'] = array('type' => 'wsclient_simplegeo_accepted', 'label' => '202');
  $service->operations['record_delete_single'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'POST';
  $operation['label'] = 'Records - Create or Update Multiple Records';
  $operation['url'] = '0.1/records/@layer.json';
  $operation['data'] = 'records';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter']['records'] = array('type' => 'records', 'label' => 'Records');
  $operation['result'] = array('type' => 'wsclient_simplegeo_accepted', 'label' => 'HTTP 202');
  $service->operations['records_multiple'] = $operation;
  $services[$service->name] = $service;

  $record_params = array(
    'lat' =>        array('type' => 'float', 'label' => 'Longitude'),
    'lon' =>        array('type' => 'float', 'label' => 'Latitude'),
    'address' =>    array('type' => 'text', 'label' => 'An Address'),
    'bbox' =>       array('type' => 'text', 'label' => 'A Bounding Box Query'),
    'geohash' =>    array('type' => 'text', 'label' => 'A GeoHash'),
    'ip' =>         array('type' => 'text', 'label' => 'IP Address'),
    'radius' =>     array('type' => 'integer', 'label' => 'Radius'),
    'limit' =>      array('type' => 'integer', 'label' => 'Number of Results'),
    'start' =>      array('type' => 'date', 'label' => 'Start Time'),
    'end' =>        array('type' => 'date', 'label' => 'End Time'),
    'order' =>      array('type' => 'integer', 'label' => 'The order in which results should be returned'), // Options are +created, -created, +property, and -property.
    'cursor' =>     array('type' => 'text', 'label' => 'Paginate to the next page of records'),
    'prop_type' =>  array('type' => 'text', 'label' => 'The type of property you wish to query'),
    'prop_name' =>  array('type' => 'text', 'label' => 'The name of the property to be queried'),
    'prop_equals' =>array('type' => 'text', 'label' => 'The property value, for exact-match property queries'),
    'prop_start' => array('type' => 'text', 'label' => 'The start value for a property range query'),
    'prop_end' =>   array('type' => 'text', 'label' => 'The end value for a property range query'),
  );

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Records - Query Nearby Records by Lat Lon';
  $operation['url'] = '0.1/records/@layer/nearby/@lat,@lon.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter'] += $record_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_record', 'label' => 'Simplegeo Records');
  $service->operations['records_nearby_latlon'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Records - Query Nearby Records by Geohash';
  $operation['url'] = '0.1/records/@layer/nearby/@geohash.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter'] += $record_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_record', 'label' => 'Simplegeo Records');
  $service->operations['records_nearby_geohash'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Records - Query Nearby Records by Address';
  $operation['url'] = '0.1/records/@layer/nearby/address.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter'] += $record_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_record', 'label' => 'Simplegeo Records');
  $service->operations['records_nearby_address'] = $operation;
  $services[$service->name] = $service;

  $operation = array();
  $operation['type'] = 'GET';
  $operation['label'] = 'Records - Query Nearby Records by IP';
  $operation['url'] = '0.1/records/@layer/nearby/@ip.json';
  $operation['parameter']['layer'] = array('type' => 'text', 'label' => 'Layer Namespace');
  $operation['parameter'] += $record_params;
  $operation['result'] = array('type' => 'wsclient_simplegeo_record', 'label' => 'Simplegeo Records');
  $service->operations['records_nearby_ip'] = $operation;
  $services[$service->name] = $service;


// @TODO: other endpoints
//
//  get History
//  add Place
//  delete Place
//  edit Place

  return $services;

}




/**
 * Helper function to create datatype array for SimpleGeo
 *
 */
function simplegeo_datatypes() {

  return array(
    'places' => array(
      'label' => 'Places Result',
      'property info' => array(
        'total' => array(
          'type' => 'integer',
          'label' => 'Total Results',
        ),
        'type' => array(
          'type' => 'text',
          'label' => 'Type of Result',
        ),
        'features' => array(
          'type' => 'list<feature>',
          'label' => 'Features list',
        ),
      ),
    ),
    'context' => array(
      'label' => 'Context Result',
      'property info' => array(
        'query' => array(
          'type' => 'location',
          'label' => 'Query Location',
        ),
        'timestamp' => array(
          'type' => 'date',
          'label' => 'Timestamp',
        ),
        'features' => array(
          'type' => 'list<contextfeature>',
          'label' => 'Features list',
        ),
        'weather' => array(
          'type' => 'weather',
          'label' => 'Weather',
        ),
        'demographics' => array(
          'type' => 'demographics',
          'label' => 'Demographics',
        ),
        'intersections' => array(
          'type' => 'list<intersection>',
          'label' => 'Intersections',
        ),
        'address' => array(
          'type' => 'address',
          'label' => 'Address',
        ),
      ),
    ),
    'feature' => array(
      'label' => 'Feature data',
      'property info' => array(
        'geometry' => array(
          'type' => 'geometry',
          'label' => 'Geometry',
        ),
       'type' => array(
          'type' => 'text',
          'label' => 'Object Type',
        ),
        'id' => array(
          'type' => 'text',
          'label' => 'SimpleGeo Handle',
        ),
        'properties' => array(
          'type' => 'featureproperties',
          'label' => 'Feature Properties',
        ),
      ),
    ),
    'geometry' => array(
      'label' => 'Geometry data',
      'property info' => array(
        'type' => array(
          'type' => 'text',
          'label' => 'Geometry Type',
        ),
        'coordinates' => array(
          'type' => 'coordinates',
          'label' => 'Geometry Coordinates',
        ),
      ),
    ),
    'featureproperties' => array(
      'label' => 'Feature Properties',
      'property info' => array(
        'website' => array(
          'type' => 'text',
          'label' => 'Website',
        ),
        'distance' => array(
          'type' => 'decimal',
          'label' => 'Distance',
        ),
        'name' => array(
          'type' => 'text',
          'label' => 'Name',
        ),
        'tags' => array(
          'type' => 'list<text>',
          'label' => 'Tags',
        ),
        'country' => array(
          'type' => 'text',
          'label' => 'Country',
        ),
        'province' => array(
          'type' => 'text',
          'label' => 'Province',
        ),
        'phone' => array(
          'type' => 'text',
          'label' => 'Phone',
        ),
        'href' => array(
          'type' => 'text',
          'label' => 'Simplegeo Link',
        ),
        'city' => array(
          'type' => 'text',
          'label' => 'City',
        ),
        'address' => array(
          'type' => 'text',
          'label' => 'Address',
        ),
        'postcode' => array(
          'type' => 'text',
          'label' => 'Postal Code',
        ),
      ),
    ),
    'coordinates' => array(
      'label' => 'Coordinate data',
      'property info' => array(
        'coordinates' => array(
          'type' => 'list<decimal>',
          'label' => 'Coordinate Data',
        ),
      ),
    ),
    'location' => array(
      'label' => 'Location data',
      'property info' => array(
        'latitude' => array(
          'type' => 'decimal',
          'label' => 'Latitude',
        ),
        'longitude' => array(
          'type' => 'decimal',
          'label' => 'Longitude',
        ),
        'ip' => array(
          'type' => 'text',
          'label' => 'IP Address',
        ),
      ),
    ),


    //@TODO
    'contextfeature' => array(
      'label' => 'Context Feature',
      'property info' => array(
        'handle' => array(
          'type' => 'decimal',
          'label' => 'Latitude',
        ),
        'name' => array(
          'type' => 'decimal',
          'label' => 'Longitude',
        ),
        'license' => array(
          'type' => 'text',
          'label' => 'IP Address',
        ),
        'bounds' => array(
          'type' => 'list<coordinates>',
          'label' => 'Bounds',
        ),
        'href' => array(
          'type' => 'text',
          'label' => 'SimpleGeo Link',
        ),
        'classifiers' => array(
          'type' => 'category',
          'label' => 'SimpleGeo Category',
        ),
      ),
    ),
    'category' => array(
      'label' => 'SimpleGeo Category',
      'property info' => array(
        'category' => array(
          'type' => 'text',
          'label' => 'Category',
        ),
        'type' => array(
          'type' => 'text',
          'label' => 'Type',
        ),
        'subcategory' => array(
          'type' => 'text',
          'label' => 'Subcategory',
        ),
        'category_id' => array(
          'type' => 'integer',
          'label' => 'SimpleGeo Category ID',
        ),
      ),
    ),



    //@TODO -> not implemented yet
    'weather' => array(
      'label' => 'Weather Data',
      'property info' => array(
//        'latitude' => array(
//          'type' => 'decimal',
//          'label' => 'Latitude',
//        ),
      ),
    ),
    'demographics' => array(
      'label' => 'Demographic data',
      'property info' => array(
//        'latitude' => array(
//          'type' => 'decimal',
//          'label' => 'Latitude',
//        ),
      ),
    ),
    'intersection' => array(
      'label' => 'Instersection data',
      'property info' => array(
//        'latitude' => array(
//          'type' => 'decimal',
//          'label' => 'Latitude',
//        ),
      ),
    ),
    'address' => array(
      'label' => 'Address data',
      'property info' => array(
//        'latitude' => array(
//          'type' => 'decimal',
//          'label' => 'Latitude',
//        ),
      ),
    ),
    'layer' => array(
      'label' => 'SimpleGeo Layer',
      'property info' => array(
        'name' => array(
          'type' => 'text',
          'label' => 'Layer Namespace',
        ),
        'title' => array(
          'type' => 'text',
          'label' => 'Layer Label',
        ),
        'description' => array(
          'type' => 'text',
          'label' => 'Description',
        ),
        'public' => array(
          'type' => 'boolean',
          'label' => 'Is Public',
        ),
        'created' => array(
          'type' => 'date',
          'label' => 'Created Date',
        ),
        'updated' => array(
          'type' => 'date',
          'label' => 'Updated',
        ),
      ),
    ),
    'record' => array(
      'label' => 'SimpleGeo Record',
      'property info' => array(
        'name' => array(
          'type' => 'text',
          'label' => 'Layer Namespace',
        ),
        'title' => array(
          'type' => 'text',
          'label' => 'Layer Label',
        ),
        'description' => array(
          'type' => 'text',
          'label' => 'Description',
        ),
        'public' => array(
          'type' => 'boolean',
          'label' => 'Is Public',
        ),
        'created' => array(
          'type' => 'date',
          'label' => 'Created Date',
        ),
        'updated' => array(
          'type' => 'date',
          'label' => 'Updated',
        ),
      ),

    ),

  );
}