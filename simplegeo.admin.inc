<?php




/**
 *  Simplegeo Settings form
 */
function simplegeo_admin_settings() {


  $form['simplegeo_oauth'] = array(
    '#type' => 'textfield',
    '#title' => t('SimpleGeo OAuth Key'),
    '#default_value' => variable_get('simplegeo_oauth', ''),
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t("Your SimpleGeo OAuth Key."),
    '#required' => TRUE,
    '#weight' => 0,
  );
  $form['simplegeo_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret'),
    '#default_value' => variable_get('simplegeo_secret', ''),
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t("SimpleGeo Secret."),
    '#required' => TRUE,
    '#weight' => 0,
  );
  $form['simplegeo_json_token'] = array(
    '#type' => 'textfield',
    '#title' => t('JSONP Token for your domain'),
    '#default_value' => variable_get('simplegeo_json_token', ''),
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t("SimpleGeo Secret."),
    '#required' => TRUE,
    '#weight' => 0,
  );

  return system_settings_form($form);

}

function simplegeo_layers_listing() {

  $rows = array();

  $result = simplegeo_layer_get_all();
  $layers = $result['layers'];

  foreach ($layers as $layer) {
    $rows[] = array(
      array('data' => $layer['name']),
      array('data' => $layer['title']),
      array('data' => l('view records', '<front>')),
      array('data' => l('edit', SIMPLEGEO_ADMIN_PATH .'/layers/edit/' . $layer['name'])),
      array('data' => l('clone', SIMPLEGEO_ADMIN_PATH .'/layers/clone/' . $layer['name'])),
      array('data' => l('delete', SIMPLEGEO_ADMIN_PATH .'/layers/delete/' . $layer['name'])),

    );
  }
  // Assemble the right table header.
  $header = array('name' => t('Layer Namespace'), 'title' => t('Layer Title'), array('data' => t('Operations'), 'colspan' => 4));

  $render = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Layers.'),
  );


  return $render;
}


function simplegeo_layer_router($op, $namespace = NULL) {

  switch($op) {
    case "add":
      return drupal_get_form('simplegeo_layers_edit', array('op' => $op));
      break;
    case "edit":
      $layer = simplegeo_layer('get', $namespace);
      $layer['op'] = $op;
      return drupal_get_form('simplegeo_layers_edit', $layer);
      break;
    case "clone":
      $layer = simplegeo_layer('get', $namespace);
      $layer['name'] = $layer['name'] . '.clone';
      $layer['title'] = $layer['title'] . ' Clone';
      $layer['op'] = $op;
      return drupal_get_form('simplegeo_layers_edit', $layer);
      break;
    case "delete":
      return drupal_get_from('simplegeo_layers_delete_form', $namespace);
      break;
  }

}


function simplegeo_layers_edit($form, &$form_state, $layer) {
  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Layer Namespace'),
    '#default_value' => isset($layer['name']) ? $layer['name'] : '',
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t("SimpleGeo Layer Namespace, in !link format.", array('!link' => l('RDN', 'http://en.wikipedia.org/wiki/Reverse-DNS', array('absolute' => TRUE)))),
    '#required' => TRUE,
    '#weight' => 0,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Layer Title'),
    '#default_value' => isset($layer['name']) ? $layer['name'] : '',
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t("The title of this layer"),
    '#required' => TRUE,
    '#weight' => 1,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($layer['description']) ? $layer['description'] : '',
    '#description' => t("The description of this layer."),
    '#required' => TRUE,
    '#weight' => 2,
  );
  $form['public'] = array(
    '#type' => 'radios',
    '#title' => t('Privacy Settings'),
    '#default_value' => !empty($layer['public']) ? $layer['public'] : 0,
    '#description' => t("Pulic or Private Layer."),
    '#options' => array(0 => 'Private', 1 => 'Public'),
    '#required' => TRUE,
    '#weight' => 3,
  );

  $form_state['layer'] = $layer;

  $form['actions'] = array(
    '#weight' => 5,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add Layer',
  );

  if ($layer['op'] == 'edit') {
    $form['actions']['submit']['#value'] = 'Update Layer';
  }


  return $form;

}

function simplegeo_layers_edit_validate($form, &$form_state) {
  $layer = $form_state['layer'];

}

function simplegeo_layers_edit_submit($form, &$form_state) {

  //$layer = simplegeo_layer('put', $namespace, $layerdata);

}



function simplegeo_layers_delete_form($form, &$form_state, $namespace) {

  $form = array();
  $confirm_question = t('Are you sure you want to delete the layer %layer?', array('%layer' => $namespace));
  $form_state['namespace'] = $namespace;
  $form['#submit'][] = 'simplegeo_layers_delete_form_submit';
  return confirm_form($form, $confirm_question, SIMPLEGEO_ADMIN_PATH .'/layers');

}

/**
 * Submit callback for simplegeo_layers_delete_form
 */
function simplegeo_layers_delete_form_submit($form, &$form_state) {
  $namespace = $form_state['namespace'];

  if ($result = simplegeo_layer('delete', $namespace)) {
    drupal_set_message(t('The namespace %namespace has been deleted.', array('%namespace' => $namespace)));
    watchdog('simplegeo', 'Deleted namespace %namespace.', array('%namespace' => $namepsace));
  }
  else {
    drupal_set_message(t('There was an error deleting namespace %namespace.', array('%namespace' => $namespace)));
    watchdog('simplegeo', 'Error deleting namespace %namespace.', array('%namespace' => $namepsace), $result);
  }

  $form_state['redirect'] = SIMPLEGEO_ADMIN_PATH .'/layers';
}




