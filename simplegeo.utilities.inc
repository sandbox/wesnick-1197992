<?php


/*
 * @file Utility functions for simplegeo
 */

function simplegeo_get_layer_namespaces() {

  $namespaces = array();
  $cache = cache_get("simplegeo:namespaces", 'cache');
  if (isset($cache->data)) {
    $namespaces = $cache->data;
  }
  else {
    if ($service = wsclient_service_load('simplegeo')) {
      $next_cursor = NULL;
      do {
        $results = simplegeo_layer_get_all(50, $next_cursor);
        foreach ($results['layers'] as $layer) {
          $namespaces[$layer['name']] = $layer['title'];
        }
        $next_cursor = $results['next_cursor'];

      } while (!empty($next_cursor));

      cache_set("simplegeo:namespaces", $namespaces, CACHE_TEMPORARY);
    }
  }
  return $namespaces;

}


function simplegeo_get_bounding_box($address, $category_type, $category) {

  $result = simplegeo_context_get_address(array('address' => $address));

  $bounding_box = array();

  foreach($result['features'] as $feature) {
    if ($feature['classifiers'][0][$category_type] == $category) {
      $bounding_box['name'] = $feature['name'];
      $bounding_box['ne_lon'] = $feature['bounds'][0];
      $bounding_box['ne_lat'] = $feature['bounds'][3];
      $bounding_box['sw_lon'] = $feature['bounds'][2];
      $bounding_box['sw_lat'] = $feature['bounds'][1];
      $bounding_box['handle'] = $feature['handle'];
    }
  }

  return $bounding_box;
}


/**
 * Return an array of the closest intersection and cross streets
 * @param $result
 * @return unknown_type
 */
function simplegeo_context_result_parse_intersections($result) {

  $intersections = array();
  $corner_counter = 0;
  if (!is_array($result['intersections'])) return;

  foreach($result['intersections'] as $int) {
    $current[$corner_counter]['distance'] = $int['distance'];
    foreach($int['properties']['highways'] as $id => $corners) {
      if ($id % 2) {
        $current[$corner_counter]['EW'] = $corners['name'];
      } else {
        $current[$corner_counter]['NS'] = $corners['name'];
      }
      // Generate our between street blurb
      if ($corner_counter == 1 && $id == 1) {
        // If these streets are the same, then it is between the others
        if ($current[0]['EW'] == $current[1]['EW']) {
          $cross['type'] = "Between";
          $cross[0] = $current[0]['NS'];
          $cross[1] = $current[1]['NS'];
        }
        if ($current[0]['NS'] == $current[1]['NS']) {
          $cross['type'] = "Between";
          $cross[0] = $current[0]['EW'];
          $cross[1] = $current[1]['EW'];
        }

        if (!isset($cross)) {
          $cross['type'] = "Corner of";
          $cross[0] = $current[0]['NS'];
          $cross[1] = $current[0]['EW'];

        }

        $current['cross'] = $cross;
        $current['crosstext'] = $cross['type'] . ' ' . $cross[0] . ' and ' . $cross[1];

      }
    }

    $corner_counter += 1;
  }

  return $current;

}